**README**

Python 3.10.2

**Установка**

    Склонируйте репозиторий: git clone https://gitlab.com/dvtarkov/uptrader-test.git


    Установите зависимости: pip install -r requirements.txt


    Примените миграции: python manage.py migrate



**Использование**

!!! Unit тесты draw_menu доступны по команде 
`python manage.py test menu_app.tests`
Содержит два теста - проверка отрисовки пунктов меню и проверка, что количество запросов к БД не превышает 1.



Создание меню

Меню можно создавать через стандартную админку Django. Для этого нужно зайти на страницу http://localhost:8000/admin/menu_app/menu/, где http://localhost:8000 - адрес вашего приложения. На этой странице вы можете создать новое меню, указав его название и элементы меню. Элементы меню можно создавать в админке, либо привязывать к уже существующим моделям и их URL.


Отображение меню

Чтобы отобразить меню на странице, нужно загрузить template tag:

`{% load menu_tags %}`

И затем в нужном месте вызвать

`{% draw_menu 'main_menu' %}`

где main_menu - название меню.
Тег draw_menu будет рисовать древовидное меню, основываясь на элементах, созданных в админке.

    Все, что над выделенным пунктом - развернуто. Первый уровень вложенности под выделенным пунктом тоже развернут.
    
    Активный пункт меню определяется исходя из URL текущей страницы
    
    Меню на одной странице может быть несколько. Они определяются по названию.
    
    При клике на меню происходит переход по заданному в нем URL. URL может быть задан как явным образом, так и через named url.
    
    На отрисовку каждого меню требуется ровно 1 запрос к БД
